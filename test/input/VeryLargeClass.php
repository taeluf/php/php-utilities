
<?php

namespace Lia\Addon;

/**
 *
 * @see Router tests for good examples of output
 *
 * @note `$routeMap` contains all the info needed for selecting which route to use
 * @note optional paramaters (`/blog/{?optional}/`) only allow for one optional paramater
 *
 * @todo handleBlog(){} to respond to a request, routeBlog(){} to get an array of routes (which auto-point to handleBlog(){}))
 */
class Router extends \Lia\Addon implements \Lia\Http\ResponseInterface {


    /**
     * Array of routers. Typically, one for each app
     */
    protected array $routers = [];

    /**
     * Setup routes for an app.
     *
     * @param $app
     * @param $value true to enable with default of 'public'. false to NOT enable. string to specify directory to use for route setup
     */
    public function onAppEnabled(\Lia\AppInterface $app, mixed $value){
        if ($value===true)$value = 'public';
        else if ($value===false)return;
        else if (!is_string($value)){
            $ns = $app->getNamespace();
            $class = get_class($app);
            $type = gettype($value);
            throw new \Exception("App '$ns' (class '$class') enabled addon 'lia:router', but provided an invalid value. The value MUST be a boolean or string, but a '$type' was provided. The value was '$value'");
        }

        // actually setup the routes

        $router = new \Lia\Src\Router();
        // @TODO configure params to pass to routes, on an app-by-app basis
        // @TODO configure varDelim, on an app-by-app basis
        $base_url = $app->hasConfig('router.base_url') ? $app->getConfig('router.base_url') : '/';
        $router->addDirectoryRoutes($app->getPath($value), $base_url);

        $this->routers[] = $router;
    }

    public function onGetHttpRoutes(\Lia\Http\Request $request): array {
        // @TODO consider hooking each Lia\Src\Router instance into GetHttpRoutes, instead of managing them within the addon.
    
        $route_list = [];
        /** @var $router \Lia\Src\Router */
        foreach ($this->routers as $router){
            $route_list[] = $router->getRoute($request);
        }

        return $route_list;
        
    }

    public function onAddonsLoaded(\Lia\AppInterface $app, array $addons){
        // @TODO maybe add a different callback for an addon to set itself up

        $app->getLia()->hook(\Lia\Events::GetHttpRoutes->value, [$this, 'onGetHttpRoutes']);
        $app->getLia()->hook(\Lia\Events::NoHttpRoutes->value, [$this, 'onNoHttpRoutes']);
        $app->getLia()->hook(\Lia\Events::NoHttpRoutes->value, [$this, 'onNoHttpRoutes']);
        $app->getLia()->hook(\Lia\Events::MultipleHttpRoutes->value, [$this, 'onNoSingleHttpRoute']);

    }

    /** do nothing */
    public function onRequestReady(\Lia\Http\Request $request){}

    /** Send a response */
    public function onNoHttpRoutes(\Lia\Http\Request $request){}

    /** 
     * Send a response 
     * @param $routes array<int index, \Lia\Http\Route $route> an array of Routes
     */
    public function onMultipleHttpRoutes(array $routes){

    }

    /**
     * Send a response
     * @param $route \Lia\Http\Route 
     */
    public function onSingleHttpRoute(\Lia\Http\Route $route){

    }

    /**
     * 
     * @param $request The request that is finished
     */
    public function onRequestFinished(\Lia\Http\Request $request){

    }


    /**
     * A string of characters that can be used as delimiters for dynamic url portions
     * @note this setting is global across all routes. You can circumvent this by using a second router addon
     */
    public $varDelim = '\\.\\/\\-\\:';
    // public $varDelim = '\\.\\/\\:';

    /**
     * @structure `['GET'=>['/test/?/pattern/'=>$decoded_pattern_array] ...]` 
     */
    public $routeMap = [];


    /**
     * Clean a url (without domain): replace space with '+', single quote with '%27', and replace multiple slashes with a single slash
     *
     * @param $dirty_url a url like `/some url///'with quotes'//`
     * @return a cleaned up url
     *
     * @example `/some url///'with quotes'//` returns `/some+url/%27with+quotes%27/`
     */
    public function clean_url($dirty_url){
        $dirty_url = str_replace(' ', '+', $dirty_url);
        $dirty_url = str_replace('\'', '%27', $dirty_url);
        $dirty_url = str_replace(['///','//'],'/',$dirty_url);
        return $dirty_url;
    }

    /**
     * This method is not tested at all. does not check dynamic routes.
     * @return true/false if the path has a route.
     */
    public function has_static_route(string $path,string $method="GET"):bool{
        return isset($this->routeMap[$method][$path]);
    }

    /**
     * Scan for files in the given directory & set each file up as a route.
     */
    public function addDirectoryRoutes(string $dir, string $base_url){

        $patterns = $this->dir_to_patterns($dir);
        foreach ($patterns as $file=>$pattern){
            $full_pattern = str_replace('//','/',$base_url.$pattern);


            // @TODO Setup router class to pass paramaters to routes (probably pass the same params to ALL routes within a given router)
            $this->addRoute($full_pattern,$dir.'/'.$file, null);
        }

    }


    /**
     * Add a route
     * 
     * @param $pattern A pattern. See decode_pattern()  documentation
     * @param $callbackOrFile a callback or a file path
     * @param $package (optional) A liaison package
     *
     */
    public function addRoute($pattern, $callbackOrFile,$package=null){
        $initialParsed = $this->decode_pattern($pattern);
        $list = $this->separate_optional_from_decoded_pattern($initialParsed);
        foreach ($list as $decoded){
            $decoded['target'] = $callbackOrFile;
            $decoded['package'] = $package;
            $testPattern = $decoded['parsedPattern'];
            $matches = [];
            foreach ($decoded['methods'] as $m){
                $this->routeMap[$m][$testPattern][] = $decoded;
            }            
        }
    }

    /**
     *  get a route for the given request
     *
     *  @todo write test for routing via added routers
     */
    public function getRoute(\Lia\HttpRequest $request){
        $url = $request->url();
        $method = $request->method();

        $testReg = $this->url_to_regex($url);
        $all = array_filter($this->routeMap[$method]??[],
            function($routeList,$decoded_pattern) use ($testReg) {
                if (preg_match('/'.$testReg.'/',$decoded_pattern))return true;
                return false;
            }
            ,ARRAY_FILTER_USE_BOTH);
        $routeList = [];
        sort($all);
        $all = array_merge(...$all);
        foreach ($all as $routeInfo){
            $active = [
                'url' => $url,
                'method'=>$method,
                'urlRegex'=>$testReg
            ];
            $paramaters = null;
            $paramaters = $this->extract_url_paramaters($routeInfo, $url);
            $optionalParamaters = $routeInfo['optionalParams']??[];
            $shared = [
                'paramaters'=>$paramaters,
                'optionalParamaters'=> $optionalParamaters,
            ];
            $static = [
                'allowedMethods'=>$routeInfo['methods'],
                'paramaterizedPattern'=>$routeInfo['pattern'],
                'placeholderPattern'=>$routeInfo['parsedPattern'],
                'target'=>$routeInfo['target'],
                'package'=>$routeInfo['package'],
            ];
            $route = new \Lia\Obj\Route(array_merge($active,$shared,$static));
            $routeList[] = $route;
        }
        return $routeList;
    }

    /**
     * Get an array of patterns from files in a directory.
     *
     * @return key=>value array, like `[rel_file_path=>pattern]` 
     */
    protected function dir_to_patterns($dir, $prefix=''){
        $files = \Lia\Utility\Files::all($dir,$dir);
        $patterns = [];
        foreach ($files as $f){
            $patterns[$f] = $prefix.$this->fileToPattern($f);
        }

        return $patterns;
    }

    /**
     * Convert a relative file path into a pattern
     *
     * @todo move file path => pattern conversion into router
     * @tag utility, routing
     */
    protected function fileToPattern($relFile){
        $pattern = $relFile;
        $ext = pathinfo($relFile,PATHINFO_EXTENSION);
        // $hidden = $this->props['route']['hidden_extensions'];
        $hidden = ['php'];
        if (in_array($ext,$hidden)){
            $pattern = substr($pattern,0,-(strlen($ext)+1));
            $ext = '';
        }

        // $indexNames = $this->props['route']['index_names'];
        $indexNames = ['index'];
        $base = basename($pattern);
        if (in_array($base,$indexNames)){
            $pattern = substr($pattern,0,-strlen($base));
        }

        // if ($ext==''
            // &&$this->config['route']['force_trail_slash'])$pattern .= '/';
        if ($ext==''
            &&true)$pattern .= '/';

        $pattern = str_replace(['///','//'], '/', $pattern);
        return $pattern;
    }



    /**
     * Facilitates optional paramaters
     *
     * Processes a parsed pattern into an array of valid parsed patterns where the original pattern may contain details for optional paramaters
     *
     * @return array of valid patterns (including the original)
     */
    protected function separate_optional_from_decoded_pattern($original_parsed){
        $clean_original = $original_parsed;
        unset($clean_original['extraParsedPattern']);
        unset($clean_original['optionalParams']);

        $list = [$clean_original];
        if (isset($original_parsed['extraParsedPattern'])){
            $next_parsed = $clean_original;
            $next_parsed['parsedPattern'] = $original_parsed['extraParsedPattern'];
            $params = $clean_original['params'];
            foreach ($original_parsed['optionalParams'] as $p){
                $index = array_search($p, $params);
                unset($params[$index]);
            }
            
            $params = array_values($params);
            $next_parsed['params'] = $params;
            $list[] = $next_parsed;
        }
        return $list;
    }

    /**
     *
     * Convert a pattern into a decoded array of information about that pattern
     *
    * The patterns apply both for the `public` dir and by adding routes via `$lia->addRoute()`. The file extension (for .php) is removed prior to calling decode_pattern()
    * The delimiters can be changed globally by setting $router->varDelims
    * 
    * ## Examples: 
    * - /blog/{category}/{post} is valid for url /blog/black-lives/matter
    * - /blog/{category}.{post}/ is valid for url /blog/environment.zero-waste/
    * - /blog/{category}{post}/ is valid for url /blog/{category}{post}/ and has NO dynamic paramaters
    * - /blog/{category}/@GET.{post}/ is valid for GET /blog/kindness/is-awesome/ but not for POST request
    * - /@POST.dir/sub/@GET.file/ is valid for both POST /dir/sub/file/ and GET /dir/sub/file/
    *
    * ## Methods: @POST, @GET, @PUT, @DELETE, @OPTIONS, @TRACE, @HEAD, @CONNECT
    * - We do not currently check the name of the method, just @ABCDEF for length 3-7
    * - These must appear after a `/` or after another '@METHOD.' or they will be taken literally
    * - lower case is not valid
    * - Each method MUST be followed by a period (.)
    * 
    * ## Paramaters:
    * - NEW: Dynamic portions may be separated by by (-) and/or (:) 
    * - {under_scoreCamel} specifies a named, dynamic paramater
    * - {param} must be surrounded by path delimiters (/) OR periods (.) which will be literal characters in the url
    * - {param} MAY be at the end of a pattern with no trailing delimiter
    *
    * ## TODO
    * - {paramName:regex} would specify a dynamic portion of a url that MUST match the given regex. 
    *     - Not currently implemented
    * - {?param} would specify a paramater that is optional
    *     - Not currently implemented
    *
    * @export(Router.PatternRules)
    */
    protected function decode_pattern($pattern){

        $dlm = $this->varDelim;

        $params = [];
        $optionalParams = [];
        $replace = 
        function($matches) use (&$params, &$optionalParams){
            if ($matches[1]=='?'){
                $params[] = $matches[2];
                $optionalParams[] = $matches[2];
                return '#';                
            }
            $params[] = $matches[2];
            return '?';
        };
        $pieces = explode('/',$pattern);
        $methods = [];
        $testUrl = '';
        $extraTestUrl = '';
        foreach ($pieces as $piece){
            $startPiece = $piece;
            // extract @METHODS.
            while (preg_match('/^\@([A-Z]{3,7})\./',$piece,$methodMatches)){
                $method = $methodMatches[1];
                $len = strlen($method);
                $piece = substr($piece,2+$len);
                $methods[$methodMatches[1]] = $methodMatches[1];
            } 
            while ($piece!=($piece = preg_replace_callback('/(?<=^|['.$dlm.'])\{(\??)([a-zA-Z\_]+)\}(?=['.$dlm.']|$)/',$replace,$piece))){
            }
            if ($piece=='#'&&$startPiece!=$piece){
                $extraTestUrl .= '';// don't add anything.
                $piece = '?';
            } else {
                $extraTestUrl .= '/'.$piece;
            }
            $testUrl .= '/'.$piece;
        }

        $testUrl = str_replace(['///','//'],'/',$testUrl);
        $extraTestUrl = str_replace(['///','//'],'/',$extraTestUrl);
        
        $decoded = [
            'pattern'=>$pattern,
            'parsedPattern'=>$testUrl,
            'params'=>$params,
            'methods'=>count($methods)>0 ? $methods : ['GET'=>'GET'],
        ];
        
        if ($testUrl!=$extraTestUrl){
            $decoded['extraParsedPattern']=$extraTestUrl;
            $decoded['optionalParams'] = $optionalParams;
        }
        return $decoded;
    }

    /**
     * Given a url and array of paramaters, 
     *
     * @param $decoded_pattern expects the array generated by decode_pattern(/url/pattern/)
     * @param $url 
     *
     * @return an array of paramaters expected by $decoded_pattern and found in $url
     */
    protected function extract_url_paramaters($decoded_pattern, $url){

        $phPattern = $decoded_pattern['parsedPattern'];
        $staticPieces = explode('?',$phPattern);
        $staticPieces = array_map(function($piece){return preg_quote($piece,'/');}, $staticPieces);
        $dlm = $this->varDelim;
        $reg = "([^{$dlm}].*)";
        $asReg = '/'.implode($reg, $staticPieces).'/';
        preg_match($asReg,$url,$matches);
        $params = [];
        $i=1;
        foreach ($decoded_pattern['params'] as $name){
            $params[$name] = $matches[$i++] ?? null;
            if ($params[$name]==null){
                echo "\n\nInternal Error. Please report a bug on https://github.com/Taeluf/Liaison/issues with the following:\n\n";
                echo "url: {$url}\nParsed Pattern:\n";
                print_r($decoded_pattern);
                echo "\n\n";
                throw new \Lia\Exception("Internal error. We were unable to perform routing for '{$url}'. ");
            }
        }

        return $params;
    }

    /**
     * convert an actual url into regex that can be used to match the test regs.
     *
     * @example `/one/two/` becomes `^\/(?:one|\?)\/(?:two|\?)\/$`
     */
    protected function url_to_regex($url){
        $url = str_replace('+', ' ',$url);
        $dlm = $this->varDelim;
        $pieces = preg_split('/['.$dlm.']/',$url);
        array_shift($pieces);
        $last = array_pop($pieces);
        if ($last!='')$pieces[] = $last;
        $reg = '';
        $pos = 0;

        $test = '';
        foreach ($pieces as $p){
            $len = strlen($p)+1;
            $startDelim = substr($url,$pos,1);
            if ($p==''){
                $test .= '\\'.$startDelim;
                $pos += $len;
                continue;
            }
            $pEsc = preg_quote($p,'/');
            $pReg = '\\'.$startDelim.'(?:'.$pEsc.'|\?)';

            $pos += $len;
            $test .= $pReg;
        }
        $finalDelim = substr($url,$pos);
        $test .= $finalDelim ? '\\'.$finalDelim : '';
        $test = '^'.$test.'$';
        return $test;
    }

    /** 
     * Get a url from a parsed pattern & array of values to fill
     *
     * @param $decoded see decode_pattern()
     * @param $withValues a `key=>value` array
     * @return the url with the paramaters inserted
     */ 
    protected function decoded_pattern_to_url(array $decoded, array $withValues): string{
        $sorted = [];
        foreach ($decoded['params'] as $index=>$param){
            $sorted[$index] = $withValues[$param];
        }
        $filledPattern = $decoded['parsedPattern'];
        $val = reset($sorted);
        while($pos = strpos($filledPattern,'?')){
            $filledPattern = substr($filledPattern,0,$pos)
                .$val
                .substr($filledPattern,$pos+1);
            $val = next($sorted);
        }
        
        return $filledPattern;
    }
}
