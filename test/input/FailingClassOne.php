<?php

namespace Lia\Addon;

/**
 *
 * @see Router tests for good examples of output
 *
 * @note `$varDelim` is a string of characters used to separate dynamic portions of a url.
 * @note `$varDelim` is a global setting, but you can change it, add routes, change it, add routes, etc.
 * @note `pattern` refers to an unparsed url pattern like `/{category}/{blog_name}/`
 * @note added routers take a `\Lia\Obj\Request` object
 * @note route derivers accept `false` and return array of patterns or a `\Lia\Obj\Request` object
 * @note test patterns (or testReg) are simplified patterns with `?` in place of dynamic paramater names & are used internally
 * @note `$routeMap` contains all the info needed for selecting which route to use
 * @note optional paramaters (`/blog/{?optional}/`) only allow for one optional paramater
 *
 * @todo handleBlog(){} to respond to a request, routeBlog(){} to get an array of routes (which auto-point to handleBlog(){}))
 */
class Router extends \Lia\Addon implements \Lia\Http\ResponseInterface {

}
