#!/usr/bin/env php
<?php

$root = dirname(__DIR__,2);
require_once($root.'/code/Util.php');

/**
 * php tester relies upon the class finder method, SO this file tests that method
 */

$file = $root.'/test/input/SampleClass.php';

$actual_class = \Tlf\Util::getClassFromFile($file);

$target_class = '\\Tlf\\Util\\Test\\SampleClass';


echo "\nTarget Class: '$target_class'";
echo "\nClass Found: '$actual_class'";
