<?php

namespace Tlf\Util\Test;

class ClassFinder extends \Tlf\Tester {

    public function testGetClassWithUse(){
        $file = $this->file('test/input/ClassWithUse.php');
        
        $actual_class = \Tlf\Util::getClassFromFile($file);

        $target_class = '\\Tlf\\Util\\Test\\ClassWithUse';

        $this->compare($target_class, $actual_class);

    }
    public function testClassOnly(){

        $file = $this->file('test/input/ClassOnlyTest.php');
        
        $actual_class = \Tlf\Util::getClassFromFile($file);

        $target_class = '\\ClassOnlyTest';

        $this->compare($target_class, $actual_class);
    }

    public function testClassWithNamespace(){

        $file = $this->file('test/input/ClassWithNamespace.php');
        
        $actual_class = \Tlf\Util::getClassFromFile($file);

        $target_class = '\\Tlf\\Util\\Test\\ClassWithNamespace';

        $this->compare($target_class, $actual_class);
    }

    public function testClassExtends(){

        $file = $this->file('test/input/ClassExtends.php');
        
        $actual_class = \Tlf\Util::getClassFromFile($file);

        $target_class = '\\Tlf\\Util\\Test\\ClassExtends';

        $this->compare($target_class, $actual_class);
    }


    public function testClassImplements(){
        $file = $this->file('test/input/ClassImplements.php');
        
        $actual_class = \Tlf\Util::getClassFromFile($file);

        $target_class = '\\Tlf\\Util\\Test\\ClassImplements';

        $this->compare($target_class, $actual_class);
    }
    
    public function testClassExtendsAndImplements(){
        $file = $this->file('test/input/ClassExtendsAndImplements.php');
        
        $actual_class = \Tlf\Util::getClassFromFile($file);

        $target_class = '\\Tlf\\Util\\Test\\ClassExtendsAndImplements';

        $this->compare($target_class, $actual_class);
    }

    public function testClassExtendsAndImplementsNoNamespace(){
        $file = $this->file('test/input/ClassExtendsAndImplementsNoNamespace.php');
        
        $actual_class = \Tlf\Util::getClassFromFile($file);

        $target_class = '\\ClassExtendsAndImplementsNoNamespace';

        $this->compare($target_class, $actual_class);
    }

    /**
     * A class in Liaison was failing. The docblock was very large, and the fread() approach was not getting enough of the file contents.
     * I tried debugging & fixing, but was unsuccessfull & switched to simple file_get_contents(), so the full file content is always loaded now, unfortunately.
     *
     * Either way, the bug is fixed.
     */
    public function testFailingClassOne(){
        $file = $this->file('test/input/FailingClassOne.php');
        
        $actual_class = \Tlf\Util::getClassFromFile($file);

        $target_class = '\\Lia\\Addon\\Router';

        $this->compare($target_class, $actual_class);
    }

    public function testVeryLargeClass(){
        $file = $this->file('test/input/VeryLargeClass.php');
        
        $actual_class = \Tlf\Util::getClassFromFile($file);

        $target_class = '\\Lia\\Addon\\Router';

        $this->compare($target_class, $actual_class);
    }

    /**
     *
     * @bug if there's a block defined before the class AND the class has a lot of text before the class is defined, it's very likely not enough of the file will be loaded into the buffer, so the class will not be detected. This will not be fixed until it's a problem for me personally, because I expect this to be very rare.
     */
    public function testClassWithFunctionBefore(){
        $this->disable();
        $file = $this->file('test/input/ClassWithFunctionBefore.php');
        
        $actual_class = \Tlf\Util::getClassFromFile($file);

        $target_class = '\\Lia\\Addon\\Router';

        $this->compare($target_class, $actual_class);
    }

    public function testFailingClassOneDebug(){
        $this->disable();
        return;
        echo "\n\nDEBUG WHAT THE HECK\n\n";
        $file = $this->file('test/input/FailingClassOne.php');


        $content = file_get_contents($file);
        $tokens = token_get_all($content);


        //print_r($tokens);
        
        //exit;
    }

}
