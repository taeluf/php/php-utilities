<?php

namespace Tlf\Util\Test;

/**
 * Test CSRF Tokens
 */
class Csrf extends \Tlf\Tester {

    /**
     * Test that a POST fails if no CSRF token is passed.
     */
    public function testPostNoCsrf(){
        $bad_response = $this->post('/csrf-test-post.php', [],
        );
        echo $bad_response;

        $this->str_contains(
            $bad_response,
            'csrf post test not valid' 
        );

    }

    /**
     * @test that a request fails without a valid CSRF 
     * @test that a request succeeds with a valid CSRF
     */
    public function testValidateCsrf(){
        $response = $this->curl_post('/csrf-test.php');
        $content = $response['body'];
        $session_id = $response['cookies']['PHPSESSID']['value'];
        $csrf = json_decode($content, true);

        $bad_response = $this->curl_post('/csrf-test-post.php', [$csrf['key']=>null],
            $files=[],'backward_compat',
            [
               'REFERER: '.$this->cli->get_server_host(),
               'Cookie'=> 'PHPSESSID='.$session_id,
            ]
        );

        $this->str_contains(
            $bad_response['body'],
            'csrf post test not valid'
        );
        echo "\n\nInvalid Response:\n".$bad_response['body'];

        $good_response = $this->curl_post('/csrf-test-post.php', [$csrf['key']=>$csrf['code']],
            $files=[],'backward_compat',
            [
               'REFERER: '.$this->cli->get_server_host(),
               'Cookie'=> 'PHPSESSID='.$session_id,
            ]
        );

        $this->str_contains($good_response['body'],
            'csrf post test success'
        );

        echo "\n\nGood Response:\n".$good_response['body'];
    }

    /**
     * Test that a csrf code is successfully generated on the server
     */
    public function testGenCsrf(){
        $content = $this->get('/csrf-test.php');
        $csrf = json_decode($content, true);

        $this->is_string($csrf['code']);
        $this->is_true($csrf['expires_at'] > time());
        $this->is_true($csrf['expires_at'] < time() + 60 * 10 + 1);
        $this->compare($csrf['uri'], '/csrf-test-post.php');

    }

}
