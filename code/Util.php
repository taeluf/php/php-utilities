<?php

namespace Tlf;

/**
 * A Utility class of convenience methods
 */
class Util  {

    /**
     * use `token_get_all()` to get a fully qualified class name from a file. Only gets the first defined class. Works with both php 7.4 and php 8.0
     *
     * @param $file an absolute file path
     *
     * @param $file an absolute path to a file
     * @return null if no class found or a string with the `Qualified\Clazz\Name`
     */
    static public function getClassFromFile($file){
        $DEBUG_MODE = false;
        if (is_dir($file)||!is_file($file)){
            return null;
            // throw new \Exception("'$file' is not a file");
        }


        $version = phpversion();
        $version_int = substr($version,0,1);
        $php_version = (int)$version_int;
 //echo "\n\n\n-----------\n\n";
        if ($DEBUG_MODE){
             //echo "VERSION: $version\nINT: $php_version";
             //exit;
        }

        $fp = fopen($file, 'r');
        $class = "";
        $class = $namespace = $buffer = '';
        $i = 0;
        $has_matched_class = false;

            ob_start();

        /**
         * Ensure buffer is long enough
         * An opening bracket is returned as a string, rather than an array detailing the token like every other token
         * So if there's a value that's JUST an opening bracket, there's a pretty good chance we've found our class.
         * This fails if there are opening brackets prior to the class's opening bracket, AND the class's opening bracket doesn't get loaded into the initial buffer. Bug is noted in the test class.
         */
        do {
            $buffer .= fread($fp, 512);
            $tokens = @token_get_all($buffer);
            //print_r($tokens);
        }
        while (array_search("{", $tokens) === false && !feof($fp));
        $err = ob_get_clean();

        //print_r($tokens);
        //exit;


            // loop over tokens
        $tcount = count($tokens);
        for (;$i<$tcount;$i++) {
            if ($php_version < 8 && $tokens[$i][0] === T_NAMESPACE) {
                if ($has_matched_class)continue;
                // for php < 8, if namespace token is namespace, loop over tokens and collect T_STRING tokens as the namespaces 
                for ($j=$i+1;$j<$tcount; $j++) {
                    if ($tokens[$j][0] === T_STRING) {

                        if ($DEBUG_MODE){
                            echo "\nPHP<8 NAMESPACE MATCH";
                            var_dump($tokens[$j]);
                        }
                        $namespace .= '\\'.$tokens[$j][1];
                    } else if ($tokens[$j] === '{' || $tokens[$j] === ';') {
                        if ($DEBUG_MODE){
                            echo "\nPHP<8 BREAK on '{' or ';'";
                            var_dump($tokens[$j]);
                        }
                        break;
                    }
                }
            } else if ($php_version >= 8 && $tokens[$i][0] == T_NAME_QUALIFIED && $namespace == null){//316){ // T_NAME_QUALIFIED === 316 
                if ($has_matched_class)continue;
                if ($DEBUG_MODE){
                    echo "\nPHP8+ NAMESPACE MATCH";
                    var_dump($tokens[$i]);
                }
                // for php >= 8, T_NAME_QUALIFIED token is the full namespace
                $namespace = '\\'.$tokens[$i][1];
                // var_dump($tokens[$i]);
            }
            
            // capture class name
            if ($tokens[$i][0] === T_CLASS) {
                if ($DEBUG_MODE){
                    echo "\nCLASS KEYWORD MATCH( i=$i)\n";
                    var_dump($tokens[$i]);
                }
                for ($j=$i+1;$j<count($tokens);$j++) {
                    $entry = is_array($tokens[$j]) ? 'array' : $tokens[$j];
                    if ($DEBUG_MODE){
                        echo "\nclass subloop: $j, ".$entry;
                        var_dump($tokens[$j]);
                    }
                    if ($tokens[$j] === '{') {
                        if ($DEBUG_MODE){
                            echo "\nOPEN BRACKET MATCHED";
                            var_dump($tokens[$j]);
                        }
                        if (isset($tokens[$i+2][1])){
                            $has_matched_class = true;

                            if ($DEBUG_MODE){
                                echo "\nCLASS NAME MATCH";
                                var_dump($tokens[$i+2]);
                            }
                            $class = $tokens[$i+2][1];
                        }
                    }
                }
            }
        }

        if ($class=='')return '';
        $full = $namespace.'\\'.$class;
        // var_dump("FULL CLASS: $full");
        return $namespace.'\\'.$class;
        
    }

    /**
     * Upload a file
     * @param $file an entry in `$_FILES[]`
     * @param $destinationFolder directory to upload to
     * @param $validExts array of extensions or an array containing just `*` to accept all file types.
     * @param $maxMB max size of file to allow
     *
     * Function comes from Util class of repo at https://gitlab.com/taeluf/php/php-utilities
     */
    static public function uploadFile($file, $destinationFolder, $validExts = ['jpg', 'png'], $maxMB = 15)
    {
        // print_r($file);
        // exit;
        if (!is_array($file) || $file == []
            || $file['size'] == 0
            || $file['name'] == ''
            || $file['tmp_name'] == ''
            || !is_int($file['error'])) {
            return false;
        }

        try {
            if (!isset($file['error']) ||
                is_array($file['error'])
            ) {
                throw new \RuntimeException('Invalid parameters.');
            }

            switch ($file['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new \RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new \RuntimeException('Exceeded filesize limit.');
                default:
                    throw new \RuntimeException('Unknown errors.');
            }

            // You should also check filesize here.
            if ($file['size'] > ($maxMB * 1024 * 1024)) {
                throw new \RuntimeException('Exceeded filesize limit.');
            }

            $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
            $ext = strtolower($ext);

            // check file format
            if (!in_array('*', $validExts)&&!in_array($ext, $validExts)) {
                // var_dump($ext);
                // var_dump($validExts);
                // var_dump($file);
                // exit;
                throw new \RuntimeException('Invalid file format ('.$ext.').');
            }

            if (!file_exists($destinationFolder)) {
                mkdir($destinationFolder, 0775, true);
            }

            $fileName = sha1_file($file['tmp_name']) .'-'. uniqid() . '.' . $ext;
            if (!move_uploaded_file(
                $file['tmp_name'],
                $destinationFolder . '/' . $fileName
            )
            ) {
                throw new \RuntimeException('Failed to move uploaded file.');
            }

            return $fileName;

        } catch (\RuntimeException $e) {

            throw $e;

        }
    }

}
