<?php

namespace Tlf\Util;

/**
 * Prevent spam in forms
 */
class FormSpam {


    /** either POST or GET, depending which request method was used */
    public $data = [];

    /**
     *
     * @key the key_prefix
     * @value the actual key (with uniqid)
     */
    public array $latest_csrf = [];

    /** 
     * This should only be set after a session is validated
     * @key the csrf token name
     * @value true, always true
     */
    public array $valid_sessions = [];


    public function __construct(){
        if (count($_POST)>0)$this->data = $_POST;
        else $this->data = $_GET;
    }

    /**
     * Print all the spam controls into a form & send any cookies/headers
     *
     * @see enable_csrf() for description of args
     */
    public function print_spam_controls(string $key_prefix='',int $expiry_minutes=60, string $url_path=''){
        $this->enable_csrf($key_prefix, $expiry_minutes, $url_path);
        $this->enable_honey();
    }

    /**
     * Print the honeypot inputs
     */
    public function enable_honey(){
        require(__DIR__.'/HoneyForm.php');
    }

    /**
     * Check if the submission is valid or spam.
     *
     * @param $key_prefix @see is_valid_csrf()
     * @return true if the submission is valid, false if the submission is spam.
     */
    public function is_valid_submission(string $key_prefix=''){
        if ($this->is_valid_honey()
            && $this->is_valid_csrf($key_prefix))return true;

        return false;
    }

    /**
     * Verify the honeypot fields are correct.
     * @return true if honeypot fields are correctly filled. false means it is likely spam.
     */
    public function is_valid_honey(){
        if (!isset($this->data['honey']))return false;
        $honey = $this->data['honey'];
        $names = explode(',', $honey);
        if (count($names)!=3)return false;
        if (!isset($this->data[$names[0]])
            ||$this->data[$names[0]]!==''
            )return false;
        if (!isset($this->data[$names[1]])
            ||$this->data[$names[1]]!==''
            )return false;

        if (!isset($this->data[$names[2]])
            ||$this->data[$names[2]]==''
            )return false;

        if (!isset($this->data['honey_answer'])
            ||$this->data['honey_answer'] == ''
            )return false;
        // echo 'no';
        // exit;
        // var_dump($this->data['honey_answer']);
        // var_dump($this->data[$names[2]]);
        // var_dump(password_hash($this->data[$names[2]], PASSWORD_DEFAULT));

        // if (!password_verify($this->data[$names[2]], $this->data['honey_answer']))return false;
        if (md5($this->data[$names[2]]) != $this->data['honey_answer'])return false;

        return true;
    }

    /**
     * Checks `$_POST` for the csrf token
     *
     * @param $key_prefix the same key prefix you passed to `$this->enable_csrf()`
     * @return true if the csrf token is valid. false if it is spam.
     */
    public function is_valid_csrf(string $key_prefix=''): bool {
        // this attempts to do the checks listed on https://www.taeluf.com/blog/php/security/csrf-validation/

        $post_key = $this->get_csrf_post_key($key_prefix);
        if ($post_key=='')return false;
        $post_code = $_POST[$post_key];
        // because i unset from $_SESSION
        if (isset($this->valid_sessions[$post_key]))return true;

        if (session_status()==PHP_SESSION_NONE)session_start();
        if (session_status()!=PHP_SESSION_ACTIVE)throw new \Exception("Failed to start session. Cannot do csrf without session.");

        if (!isset($_SESSION[$post_key]))return false;
        $session_csrf = $_SESSION[$post_key];
        if ($session_csrf['code'] != $post_code) return false;
        if ($session_csrf['expires_at'] < time()) return false;
        $post_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

        if ($session_csrf['uri'] != ''
            &&$session_csrf['uri'] != $post_path
        )return false;
        if (!isset($_SERVER['HTTP_REFERER']))return false;
        $referer_domain = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
        // to remove the port (mainly bc of localhost testing)
        $server_host = parse_url($_SERVER['HTTP_HOST'], PHP_URL_HOST);
        if ($server_host==null)$server_host = $_SERVER['HTTP_HOST'];

        if ($referer_domain != $server_host)return false;

        unset($_SESSION[$post_key]);
        $this->valid_sessions[$post_key] = true;
        return true;
    }


    /**
     *
     *
     * @param $key_prefix string to help identify your csrf token.
     * @param $expiry_minutes number of minutes the token should be valid for
     * @param $url the url path the token should be validated on, like '/some/url/'. If not set, it works on any path
     *
     * @output a hidden input with the csrf value
     *
     * @return the csrf key. To load csrf data do `$_SESSION[$csrf_key]`. `$csrf_key` will be like `key_prefix-csrf-uniqid()`
     */
    public function enable_csrf(string $key_prefix='',int $expiry_minutes=60, string $url_path=''){
        $key = $key_prefix.'-csrf-'.uniqid();
        $code = $this->make_csrf_code(); 
        $data = [
            'code'=> $code,
            'expires_at' => time() + $expiry_minutes * 60,
            'uri' => $url_path,
        ];
        if (session_status()==PHP_SESSION_NONE)session_start();
        if (session_status()!=PHP_SESSION_ACTIVE)throw new \Exception("Failed to start session. Cannot do csrf without session.");
        $_SESSION[$key] = $data;
        $this->latest_csrf[$key_prefix] = $key;

        // var_dump($data);
        echo  "<input type=\"hidden\" name=\"$key\" value=\"$code\">";
        echo  "<input type=\"hidden\" name=\"csrf_key\" value=\"$key\">";
        // error_log('csrf key: '.$key);
        return $key;
    }


    public function make_csrf_code(){
        // this code from symfony csrf package: https://github.com/symfony/security-csrf/blob/5.4/TokenGenerator/UriSafeTokenGenerator.php
        $bytes = random_bytes(64);

        return rtrim(strtr(base64_encode($bytes), '+/', '-_'), '=');
    }

    /** 
     * get the key of the csrf data in `$_POST` for the given key
     * @param $key_prefix see csrf_is_valid
     */
    public function get_csrf_post_key(string $key_prefix=''): string {
        $len = strlen($key_prefix) + strlen('-csrf-');
        foreach ($_POST as $key=>$value){
            if (substr($key,0,$len)!=$key_prefix.'-csrf-')continue;
            $post_key = $key;
            // $post_code = $value;
            return $post_key;
            // break;
        }
        return '';
    }

    public function get_csrf_session_key(string $key_prefix=''): string {
        if (isset($this->latest_csrf[$key_prefix]))return $this->latest_csrf[$key_prefix];
        $len = strlen($key_prefix) + strlen('-csrf-');
        foreach ($_SESSION as $key=>$value){
            if (substr($key,0,$len)!=$key_prefix.'-csrf-')continue;
            return $key;
        }
        return '';
    }

    public function get_csrf_session_input(string $key_prefix=''): string {
        $key = $this->get_csrf_session_key($key_prefix);
        $code = $_SESSION[$key]['code'];
        return '<input type="hidden" name="'.$key.'" value="'.$code.'">';
    }

}
